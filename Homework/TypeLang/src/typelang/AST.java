package typelang;

import java.util.ArrayList;
import java.util.List;

/**
 * This class hierarchy represents expressions in the abstract syntax tree
 * manipulated by this interpreter.
 *
 * @author hridesh
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public interface AST {
    abstract class ASTNode implements AST {
        public abstract Object accept(Visitor visitor, Object env);
    }

    class Program extends ASTNode {
        List<DefineDecl> _decls;
        Exp _e;

        public Program(List<DefineDecl> decls, Exp e) {
            _decls = decls;
            _e = e;
        }

        public Exp e() {
            return _e;
        }

        public List<DefineDecl> decls() {
            return _decls;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    abstract class Exp extends ASTNode {

    }

    class VarExp extends Exp {
        String _name;

        public VarExp(String name) {
            _name = name;
        }

        public String name() {
            return _name;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    class UnitExp extends Exp {

        public UnitExp() {
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

    }

    class NumExp extends Exp {
        double _val;

        public NumExp(double v) {
            _val = v;
        }

        public double v() {
            return _val;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    class StrExp extends Exp {
        String _val;

        public StrExp(String v) {
            _val = v;
        }

        public String v() {
            return _val;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    class BoolExp extends Exp {
        boolean _val;

        public BoolExp(boolean v) {
            _val = v;
        }

        public boolean v() {
            return _val;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    abstract class CompoundArithExp extends Exp {
        List<Exp> _rest;

        public CompoundArithExp() {
            _rest = new ArrayList<>();
        }

        public CompoundArithExp(Exp fst) {
            _rest = new ArrayList<>();
            _rest.add(fst);
        }

        public CompoundArithExp(List<Exp> args) {
            _rest = new ArrayList<>();
            _rest.addAll(args);
        }

        public CompoundArithExp(Exp fst, List<Exp> rest) {
            _rest = new ArrayList<>();
            _rest.add(fst);
            _rest.addAll(rest);
        }

        public CompoundArithExp(Exp fst, Exp second) {
            _rest = new ArrayList<>();
            _rest.add(fst);
            _rest.add(second);
        }

        public Exp fst() {
            return _rest.get(0);
        }

        public Exp snd() {
            return _rest.get(1);
        }

        public List<Exp> all() {
            return _rest;
        }

        public void add(Exp e) {
            _rest.add(e);
        }

    }

    class AddExp extends CompoundArithExp {
        public AddExp(Exp fst) {
            super(fst);
        }

        public AddExp(List<Exp> args) {
            super(args);
        }

        public AddExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public AddExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    class SubExp extends CompoundArithExp {

        public SubExp(Exp fst) {
            super(fst);
        }

        public SubExp(List<Exp> args) {
            super(args);
        }

        public SubExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public SubExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    class DivExp extends CompoundArithExp {
        public DivExp(Exp fst) {
            super(fst);
        }

        public DivExp(List<Exp> args) {
            super(args);
        }

        public DivExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public DivExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    class MultExp extends CompoundArithExp {
        public MultExp(Exp fst) {
            super(fst);
        }

        public MultExp(List<Exp> args) {
            super(args);
        }

        public MultExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public MultExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A let expression has the syntax
     * <p>
     * (let ((name expression)* ) expression)
     *
     * @author hridesh
     */
    class LetExp extends Exp {
        List<String> _names;
        List<Type> _varTypes;
        List<Exp> _value_exps;
        Exp _body;

        public LetExp(List<String> names, List<Type> varTypes,
                      List<Exp> value_exps, Exp body) {
            _names = names;
            _varTypes = varTypes;
            _value_exps = value_exps;
            _body = body;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public List<String> names() {
            return _names;
        }

        public List<Type> varTypes() {
            return _varTypes;
        }

        public List<Exp> value_exps() {
            return _value_exps;
        }

        public Exp body() {
            return _body;
        }
    }

    /**
     * A define declaration has the syntax
     * <p>
     * (define name expression)
     *
     * @author hridesh
     */
    class DefineDecl extends Exp {
        String _name;
        Type _type;
        Exp _value_exp;

        public DefineDecl(String name, Type type, Exp value_exp) {
            _name = name;
            _type = type;
            _value_exp = value_exp;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public String name() {
            return _name;
        }

        public Type type() {
            return _type;
        }

        public Exp value_exp() {
            return _value_exp;
        }
    }

    /**
     * An anonymous procedure declaration has the syntax
     *
     * @author hridesh
     */
    class LambdaExp extends Exp {
        List<String> _formals;
        List<Type> _types;
        Exp _body;

        public LambdaExp(List<String> formals, List<Type> types, Exp body) {
            _formals = formals;
            _types = types;
            _body = body;
        }

        public List<String> formals() {
            return _formals;
        }

        public List<Type> types() {
            return _types;
        }

        public Exp body() {
            return _body;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A call expression has the syntax
     *
     * @author hridesh
     */
    class CallExp extends Exp {
        Exp _operator;
        List<Exp> _operands;

        public CallExp(Exp operator, List<Exp> operands) {
            _operator = operator;
            _operands = operands;
        }

        public Exp operator() {
            return _operator;
        }

        public List<Exp> operands() {
            return _operands;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * An if expression has the syntax
     * <p>
     * (if conditional_expression true_expression false_expression)
     *
     * @author hridesh
     */
    class IfExp extends Exp {
        Exp _conditional;
        Exp _then_exp;
        Exp _else_exp;

        public IfExp(Exp conditional, Exp then_exp, Exp else_exp) {
            _conditional = conditional;
            _then_exp = then_exp;
            _else_exp = else_exp;
        }

        public Exp conditional() {
            return _conditional;
        }

        public Exp then_exp() {
            return _then_exp;
        }

        public Exp else_exp() {
            return _else_exp;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A less expression has the syntax
     * <p>
     * ( < first_expression second_expression )
     *
     * @author hridesh
     */
    class LessExp extends BinaryComparator {
        public LessExp(Exp first_exp, Exp second_exp) {
            super(first_exp, second_exp);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    abstract class BinaryComparator extends Exp {
        private Exp _first_exp;
        private Exp _second_exp;

        BinaryComparator(Exp first_exp, Exp second_exp) {
            _first_exp = first_exp;
            _second_exp = second_exp;
        }

        public Exp first_exp() {
            return _first_exp;
        }

        public Exp second_exp() {
            return _second_exp;
        }
    }

    /**
     * An equal expression has the syntax
     * <p>
     * ( == first_expression second_expression )
     *
     * @author hridesh
     */
    class EqualExp extends BinaryComparator {
        public EqualExp(Exp first_exp, Exp second_exp) {
            super(first_exp, second_exp);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A greater expression has the syntax
     * <p>
     * ( > first_expression second_expression )
     *
     * @author hridesh
     */
    class GreaterExp extends BinaryComparator {
        public GreaterExp(Exp first_exp, Exp second_exp) {
            super(first_exp, second_exp);
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A car expression has the syntax
     * <p>
     * ( car expression )
     *
     * @author hridesh
     */
    class CarExp extends Exp {
        private Exp _arg;

        public CarExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A cdr expression has the syntax
     * <p>
     * ( car expression )
     *
     * @author hridesh
     */
    class CdrExp extends Exp {
        private Exp _arg;

        public CdrExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A cons expression has the syntax
     * <p>
     * ( cons expression expression )
     *
     * @author hridesh
     */
    class ConsExp extends Exp {
        private Exp _fst;
        private Exp _snd;

        public ConsExp(Exp fst, Exp snd) {
            _fst = fst;
            _snd = snd;
        }

        public Exp fst() {
            return _fst;
        }

        public Exp snd() {
            return _snd;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A list expression has the syntax
     * <p>
     * ( list expression* )
     *
     * @author hridesh
     */
    class ListExp extends Exp {
        private List<Exp> _elems;
        private Type _type;

        public ListExp(Type type, List<Exp> elems) {
            _type = type;
            _elems = elems;
        }

        public Type type() {
            return _type;
        }

        public List<Exp> elems() {
            return _elems;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A null expression has the syntax
     * <p>
     * ( null? expression )
     *
     * @author hridesh
     */
    class NullExp extends Exp {
        private Exp _arg;

        public NullExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * Eval expression: evaluate the program that is _val
     *
     * @author hridesh
     */
    class EvalExp extends Exp {
        private Exp _code;

        public EvalExp(Exp code) {
            _code = code;
        }

        public Exp code() {
            return _code;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * Read expression: reads the file that is _file
     *
     * @author hridesh
     */
    class ReadExp extends Exp {
        private Exp _file;

        public ReadExp(Exp file) {
            _file = file;
        }

        public Exp file() {
            return _file;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A letrec expression has the syntax
     * <p>
     * (letrec ((name expression)* ) expression)
     *
     * @author hridesh
     */
    class LetrecExp extends Exp {
        List<String> _names;
        List<Type> _types;
        List<Exp> _fun_exps;
        Exp _body;

        public LetrecExp(List<String> names, List<Type> types,
                         List<Exp> fun_exps, Exp body) {
            _names = names;
            _types = types;
            _fun_exps = fun_exps;
            _body = body;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public List<String> names() {
            return _names;
        }

        public List<Type> types() {
            return _types;
        }

        public List<Exp> fun_exps() {
            return _fun_exps;
        }

        public Exp body() {
            return _body;
        }
    }

    /**
     * A ref expression has the syntax
     * <p>
     * (ref expression)
     *
     * @author hridesh
     */
    class RefExp extends Exp {
        private Exp _value_exp;
        Type _type;

        public RefExp(Exp value_exp, Type type) {
            _value_exp = value_exp;
            _type = type;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public Exp value_exp() {
            return _value_exp;
        }

        public Type type() {
            return _type;
        }
    }

    /**
     * A deref expression has the syntax
     * <p>
     * (deref expression)
     *
     * @author hridesh
     */
    class DerefExp extends Exp {
        private Exp _loc_exp;

        public DerefExp(Exp loc_exp) {
            _loc_exp = loc_exp;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public Exp loc_exp() {
            return _loc_exp;
        }

    }

    /**
     * An assign expression has the syntax
     * <p>
     * (set! expression expression)
     *
     * @author hridesh
     */
    class AssignExp extends Exp {
        private Exp _lhs_exp;
        private Exp _rhs_exp;

        public AssignExp(Exp lhs_exp, Exp rhs_exp) {
            _lhs_exp = lhs_exp;
            _rhs_exp = rhs_exp;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public Exp lhs_exp() {
            return _lhs_exp;
        }

        public Exp rhs_exp() {
            return _rhs_exp;
        }

    }

    /**
     * A free expression has the syntax
     * <p>
     * (free expression)
     *
     * @author hridesh
     */
    class FreeExp extends Exp {
        private Exp _value_exp;

        public FreeExp(Exp value_exp) {
            _value_exp = value_exp;
        }

        public Object accept(Visitor visitor, Object env) {
            return visitor.visit(this, env);
        }

        public Exp value_exp() {
            return _value_exp;
        }

    }

    interface Visitor<T, U> {
        // This interface should contain a signature for each concrete AST node.
        T visit(AddExp e, U env);

        T visit(UnitExp e, U env);

        T visit(NumExp e, U env);

        T visit(StrExp e, U env);

        T visit(BoolExp e, U env);

        T visit(DivExp e, U env);

        T visit(MultExp e, U env);

        T visit(Program p, U env);

        T visit(SubExp e, U env);

        T visit(VarExp e, U env);

        T visit(LetExp e, U env); // New for the varlang

        T visit(DefineDecl d, U env); // New for the definelang

        T visit(ReadExp e, U env); // New for the funclang

        T visit(EvalExp e, U env); // New for the funclang

        T visit(LambdaExp e, U env); // New for the funclang

        T visit(CallExp e, U env); // New for the funclang

        T visit(LetrecExp e, U env); // New for the funclang

        T visit(IfExp e, U env); // Additional expressions for convenience

        T visit(LessExp e, U env); // Additional expressions for convenience

        T visit(EqualExp e, U env); // Additional expressions for convenience

        T visit(GreaterExp e, U env); // Additional expressions for convenience

        T visit(CarExp e, U env); // Additional expressions for convenience

        T visit(CdrExp e, U env); // Additional expressions for convenience

        T visit(ConsExp e, U env); // Additional expressions for convenience

        T visit(ListExp e, U env); // Additional expressions for convenience

        T visit(NullExp e, U env); // Additional expressions for convenience

        T visit(RefExp e, U env); // New for the Reflang

        T visit(DerefExp e, U env); // New for the Reflang

        T visit(AssignExp e, U env); // New for the Reflang

        T visit(FreeExp e, U env); // New for the Reflang
    }
}
