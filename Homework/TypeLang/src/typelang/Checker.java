package typelang;

import java.util.ArrayList;
import java.util.List;

import typelang.AST.*;
import typelang.Env.ExtendEnv;
import typelang.Env.GlobalEnv;
import typelang.Type.*;

public class Checker implements Visitor<Type, Env<Type>> {
    private Printer.Formatter ts = new Printer.Formatter();
    private Env<Type> initEnv = initialEnv(); //New for definelang

    private Env<Type> initialEnv() {
        GlobalEnv<Type> initEnv = new GlobalEnv<>();

		/* Type for procedure: (read <filename>). Following is same as (define read (lambda (file) (read file))) */
        List<Type> formalTypes = new ArrayList<>();
        formalTypes.add(new StringT());
        initEnv.extend("read", new FuncT(formalTypes, new StringT()));

		/* Type for procedure: (require <filename>). Following is same as (define require (lambda (file) (eval (read file)))) */
        formalTypes = new ArrayList<>();
        formalTypes.add(new StringT());
        initEnv.extend("eval", new FuncT(formalTypes, new UnitT()));

		/* Add type for new built-in procedures here */

        return initEnv;
    }

    Type check(Program p) {
        return (Type) p.accept(this, null);
    }

    public Type visit(Program p, Env<Type> env) {
        Env<Type> new_env = initEnv;

        for (DefineDecl d : p.decls()) {
            Type type = (Type) d.accept(this, new_env);

            if (type instanceof ErrorT) {
                return type;
            }

            Type dType = d.type();

            if (!type.typeEqual(dType)) {
                return new ErrorT("Expected " + dType + " found " + type + " in " + ts.visit(d, null));
            }

            new_env = new ExtendEnv<>(new_env, d.name(), dType);
        }
        return (Type) p.e().accept(this, new_env);
    }

    public Type visit(VarExp e, Env<Type> env) {
        try {
            return env.get(e.name());
        } catch (Exception ex) {
            return new ErrorT("Variable " + e.name() + " has not been declared in " + ts.visit(e, null));
        }
    }

    @SuppressWarnings("Duplicates")
    public Type visit(LetExp e, Env<Type> env) {

        List<String> names = e.names();
        List<Type> types = e.varTypes();
        List<Exp> exps = e.value_exps();

        // collect the environment
        Env<Type> new_env = env;
        for (int index = 0; index < names.size(); index++) {
            new_env = new ExtendEnv<>(new_env, names.get(index), types.get(index));
        }

        // verify the types of the variables
        for (int i = 0; i < names.size(); i++) {
            Type expType = (Type) exps.get(i).accept(this, new_env);

            if (expType instanceof ErrorT) {
                return expType;
            }

            if (!assignable(types.get(i), expType)) {
                return new ErrorT("The expected type of the " + i + " variable is " + types.get(i).tostring() + " found " + expType.tostring() + " in " + ts.visit(e, null));
            }
        }

        Type letType = (Type)  e.body().accept(this, new_env);

        if (letType instanceof ErrorT) {
            return letType;
        }

        Type bodyType = (Type) e.body().accept(this, new_env);

        if(bodyType instanceof ErrorT) {
            return bodyType;
        }

        if (letType.typeEqual(bodyType)) {
            return bodyType;
        }

        return new ErrorT("Expected " + bodyType.tostring() + " found " + letType.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(DefineDecl d, Env<Type> env) {
        String name = d.name();
        Type t = (Type) d._value_exp.accept(this, env);
        ((GlobalEnv<Type>) initEnv).extend(name, t);
        return t;
    }

    public Type visit(LambdaExp e, Env<Type> env) {
        List<String> names = e.formals();
        List<Type> types = e.types();
        String message = "The number of formal parameters and the number of "
                + "arguments in the function type do not match in ";
        if (types.size() == names.size()) {
            Env<Type> new_env = env;
            int index = 0;
            for (Type argType : types) {
                new_env = new ExtendEnv<>(new_env, names.get(index),
                        argType);
                index++;
            }
            Type bodyType = (Type) e.body().accept(this, new_env);
            return new FuncT(types, bodyType);
        }
        return new ErrorT(message + ts.visit(e, null));
    }

    public Type visit(CallExp e, Env<Type> env) {
        Exp operator = e.operator();
        List<Exp> operands = e.operands();

        Type type = (Type) operator.accept(this, env);
        if (type instanceof ErrorT) {
            return type;
        }

        String message = "Expect a function type in the call expression, found "
                + type.tostring() + " in ";
        if (type instanceof FuncT) {
            FuncT ft = (FuncT) type;

            List<Type> argTypes = ft.argTypes();
            int size_actuals = operands.size();
            int size_formals = argTypes.size();

            message = "The number of arguments expected is " + size_formals +
                    " found " + size_actuals + " in ";
            if (size_actuals == size_formals) {
                for (int i = 0; i < size_actuals; i++) {
                    Exp operand = operands.get(i);
                    Type operand_type = (Type) operand.accept(this, env);

                    if (operand_type instanceof ErrorT) {
                        return operand_type;
                    }

                    if (!assignable(argTypes.get(i), operand_type)) {
                        return new ErrorT("The expected type of the " + i +
                                " argument is " + argTypes.get(i).tostring() +
                                " found " + operand_type.tostring() + " in " +
                                ts.visit(e, null));
                    }
                }
                return ft.returnType();
            }
        }
        return new ErrorT(message + ts.visit(e, null));
    }

    public Type visit(LetrecExp e, Env<Type> env) {
        List<String> names = e.names();
        List<Type> types = e.types();
        List<Exp> fun_exps = e.fun_exps();

        // collect the environment
        Env<Type> new_env = env;
        for (int index = 0; index < names.size(); index++) {
            new_env = new ExtendEnv<>(new_env, names.get(index),
                    types.get(index));
        }

        // verify the types of the variables
        for (int index = 0; index < names.size(); index++) {
            Type type = (Type) fun_exps.get(index).accept(this, new_env);

            if (type instanceof ErrorT) {
                return type;
            }

            if (!assignable(types.get(index), type)) {
                return new ErrorT("The expected type of the " + index +
                        " variable is " + types.get(index).tostring() +
                        " found " + type.tostring() + " in " +
                        ts.visit(e, null));
            }
        }

        return (Type) e.body().accept(this, new_env);
    }

    public Type visit(IfExp e, Env<Type> env) {

        Type condType = (Type) e.conditional().accept(this, env);

        if (condType instanceof ErrorT) {
            return condType;
        }

        Type thenType = (Type) e.then_exp().accept(this, env);

        if (thenType instanceof ErrorT) {
            return thenType;
        }

        Type elseType = (Type) e.else_exp().accept(this, env);

        if (elseType instanceof ErrorT) {
            return elseType;
        }

        if (!(condType instanceof BoolT)) {
            return new ErrorT("The condition should have boolean type, found " + condType.tostring() + " in " + ts.visit(e, null));
        }

        if (thenType.typeEqual(elseType)) {
            return thenType;
        }

        return new ErrorT("The then and else expressions should have the same type, then has type " + thenType.tostring() + " and else has type " + elseType.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(CarExp e, Env<Type> env) {

        Type type = (Type) e.arg().accept(this, env);

        if (type instanceof ErrorT) {
            return type; // Just return error...
        }

        if (type instanceof PairT) {
            return ((PairT) type).fst(); // Return type for first element in pair
        }

        return new ErrorT("The car expect an expression of type Pair, found " + type.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(CdrExp e, Env<Type> env) {
        /*
		 * Your code goes here
		 */
        return new ErrorT("CdrExp has not been implemented");
    }

    public Type visit(ConsExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
        return new ErrorT("ConsExp has not been implemented");
    }

    public Type visit(ListExp e, Env<Type> env) {

        Type listType = e.type();

        for (int i = 0; i < e.elems().size(); i++) {

            Type iType = (Type) e.elems().get(i).accept(this, env);

            if (iType instanceof ErrorT) {
                return iType;
            }

            if (!listType.typeEqual(iType)) {
                return new ErrorT("The " + i + " expression should have type " + listType.tostring() + ", found " + iType.tostring() + " in " + ts.visit(e, null));
            }
        }

        return new ListT(listType);
    }

    public Type visit(NullExp e, Env<Type> env) {
		/*
		 * Your code goes here
		 */
        return new ErrorT("NullExp has not been implemented");
    }

    public Type visit(RefExp e, Env<Type> env) {

        Type refType = e.type();
        Type expType = (Type) e.value_exp().accept(this, env);

        if (refType instanceof ErrorT) {
            return refType;
        }

        if (refType.typeEqual(expType)) {
            return new RefT(refType);
        }

        return new ErrorT("The Ref expression expects type " + refType.tostring() + " found " + expType.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(DerefExp e, Env<Type> env) {

        Type type = (Type) e.loc_exp().accept(this, env);

        if (type instanceof ErrorT) {
            return type;
        }

        if (type instanceof RefT) {
            return new RefT(((RefT) type).nestType());
        }

        return new ErrorT("The dereference expression expects a reference type, found " + type.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(AssignExp e, Env<Type> env) {

        Type lhsType = (Type) e.lhs_exp().accept(this, env);
        Type rhsType = (Type) e.rhs_exp().accept(this, env);

        if (lhsType instanceof ErrorT) {
            return lhsType;
        }

        if (lhsType instanceof RefT) {
            if (rhsType instanceof ErrorT) {
                return rhsType;
            }

            Type lhsNested = ((RefT) lhsType).nestType();

            if (lhsNested.typeEqual(rhsType)) {
                return rhsType;
            }

            return new ErrorT("The inner type of the reference type is " + ((RefT) lhsType).nestType().tostring() + ", the rhs type is " + rhsType.tostring() + " in " + ts.visit(e, null));
        }

        return new ErrorT("The lhs of the assignment expression expects a reference type, found " + lhsType.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(FreeExp e, Env<Type> env) {

        Type type = (Type) e.value_exp().accept(this, env);

        if (type instanceof ErrorT) {
            return type;
        }

        if (type instanceof RefT) {
            return new UnitT();
        }

        return new ErrorT("The free expression expects a reference type found " + type.tostring() + " in " + ts.visit(e, null));
    }

    public Type visit(UnitExp e, Env<Type> env) {
        return new UnitT();
    }

    public Type visit(NumExp e, Env<Type> env) {
        return new NumT();
    }

    public Type visit(StrExp e, Env<Type> env) {
        return new StringT();
    }

    public Type visit(BoolExp e, Env<Type> env) {
        return new BoolT();
    }

    public Type visit(LessExp e, Env<Type> env) {
        return visitBinaryComparator(e, env, ts.visit(e, null));
    }

    public Type visit(EqualExp e, Env<Type> env) {
        return visitBinaryComparator(e, env, ts.visit(e, null));
    }

    public Type visit(GreaterExp e, Env<Type> env) {
        return visitBinaryComparator(e, env, ts.visit(e, null));
    }

    private Type visitBinaryComparator(BinaryComparator e, Env<Type> env, String printNode) {

        Type fstType = (Type) e.first_exp().accept(this, env);
        Type sndType = (Type) e.second_exp().accept(this, env);

        if (fstType instanceof ErrorT) {
            return fstType;
        }

        if (sndType instanceof ErrorT) {
            return sndType;
        }

        if (!(fstType instanceof NumT)) {
            return new ErrorT("The first argument of a binary expression should be num Type, found " + fstType.tostring() + " in " + printNode);
        }

        if (!(sndType instanceof NumT)) {
            return new ErrorT("The second argument of a binary expression should be num Type, found " + sndType.tostring() + " in " + printNode);
        }

        return new BoolT();
    }


    public Type visit(AddExp e, Env<Type> env) {
        return visitCompoundArithExp(e, env, ts.visit(e, null));
    }

    public Type visit(DivExp e, Env<Type> env) {
        return visitCompoundArithExp(e, env, ts.visit(e, null));
    }

    public Type visit(MultExp e, Env<Type> env) {
        return visitCompoundArithExp(e, env, ts.visit(e, null));
    }

    public Type visit(SubExp e, Env<Type> env) {
        return visitCompoundArithExp(e, env, ts.visit(e, null));
    }

    private Type visitCompoundArithExp(CompoundArithExp e, Env<Type> env, String printNode) {

        List list = e.all();

        for (Object exp : list) {
            Type expType = (Type) ((Exp) exp).accept(this, env);

            if (expType instanceof ErrorT) {
                return expType;
            }

            if (!(expType instanceof NumT)) {
                return new ErrorT("Expected num found " + expType.tostring() + " in " + printNode);
            }
        }

        return new NumT();
    }

    private static boolean assignable(Type t1, Type t2) {
        return t2 instanceof UnitT || t1.typeEqual(t2);
    }

    public Type visit(ReadExp e, Env<Type> env) {
        return UnitT.getInstance();
    }

    public Type visit(EvalExp e, Env<Type> env) {
        return UnitT.getInstance();
    }
}
