package funclang;

import java.util.List;

import funclang.AST.Exp;

public class Printer {

    public void print(Value v) {
        if (!"".equals(v.tostring()))
            System.out.println(v.tostring());
    }

    public void print(Exception e) {
        System.out.println(e.toString());
    }

    public static class Formatter implements AST.Visitor<String> {

        public String visit(AST.AddExp e, Env env) {
            StringBuilder result = new StringBuilder("(+ ");
            for (AST.Exp exp : e.all())
                result.append(exp.accept(this, env)).append(" ");
            return result + ")";
        }

        public String visit(AST.UnitExp e, Env env) {
            return "unit";
        }

        public String visit(AST.NumExp e, Env env) {
            return "" + e.v();
        }

        public String visit(AST.StrExp e, Env env) {
            return e.v();
        }

        public String visit(AST.BoolExp e, Env env) {
            if (e.v()) return "#t";
            return "#f";
        }

        public String visit(AST.DivExp e, Env env) {
            StringBuilder result = new StringBuilder("(/ ");
            for (AST.Exp exp : e.all())
                result.append(exp.accept(this, env)).append(" ");
            return result + ")";
        }

        public String visit(AST.ReadExp e, Env env) {
            return "(read " + e.file().accept(this, env) + ")";
        }

        public String visit(AST.EvalExp e, Env env) {
            return "(eval " + e.code().accept(this, env) + ")";
        }

        public String visit(AST.MultExp e, Env env) {
            StringBuilder result = new StringBuilder("(* ");
            for (AST.Exp exp : e.all())
                result.append(exp.accept(this, env)).append(" ");
            return result + ")";
        }

        public String visit(AST.Program p, Env env) {
            return "" + p.e().accept(this, env);
        }

        public String visit(AST.SubExp e, Env env) {
            StringBuilder result = new StringBuilder("(- ");
            for (AST.Exp exp : e.all())
                result.append(exp.accept(this, env)).append(" ");
            return result + ")";
        }

        public String visit(AST.VarExp e, Env env) {
            return "" + e.name();
        }

        public String visit(AST.LetExp e, Env env) {
            StringBuilder result = new StringBuilder("(let (");
            List<String> names = e.names();
            List<Exp> value_exps = e.value_exps();
            int num_decls = names.size();
            for (int i = 0; i < num_decls; i++) {
                result.append(" (");
                result.append(names.get(i)).append(" ");
                result.append(value_exps.get(i).accept(this, env)).append(")");
            }
            result.append(") ");
            result.append(e.body().accept(this, env)).append(" ");
            return result + ")";
        }

        public String visit(AST.DefineDecl d, Env env) {
            String result = "(define ";
            result += d.name() + " ";
            result += d.value_exp().accept(this, env);
            return result + ")";
        }

        public String visit(AST.LambdaExp e, Env env) {
            StringBuilder result = new StringBuilder("(lambda ( ");
            for (String formal : e.formals())
                result.append(formal).append(" ");
            result.append(") ");
            result.append(e.body().accept(this, env));
            return result + ")";
        }

        public String visit(AST.CallExp e, Env env) {
            StringBuilder result = new StringBuilder("(");
            result.append(e.operator().accept(this, env)).append(" ");
            for (AST.Exp exp : e.operands())
                result.append(exp.accept(this, env)).append(" ");
            return result + ")";
        }

        @SuppressWarnings("Duplicates")
        public String visit(AST.IfExp e, Env env) {
            String result = "(if ";
            result += e.conditional().accept(this, env) + " ";
            result += e.then_exp().accept(this, env) + " ";
            result += e.else_exp().accept(this, env);
            return result + ")";
        }

        public String visit(AST.LessExp e, Env env) {
            String result = "(< ";
            result += e.first_exp().accept(this, env) + " ";
            result += e.second_exp().accept(this, env);
            return result + ")";
        }

        public String visit(AST.EqualExp e, Env env) {
            String result = "(= ";
            result += e.first_exp().accept(this, env) + " ";
            result += e.second_exp().accept(this, env);
            return result + ")";
        }

        public String visit(AST.GreaterExp e, Env env) {
            String result = "(> ";
            result += e.first_exp().accept(this, env) + " ";
            result += e.second_exp().accept(this, env);
            return result + ")";
        }

        public String visit(AST.CarExp e, Env env) {
            String result = "(car ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.CdrExp e, Env env) {
            String result = "(cdr ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.ConsExp e, Env env) {
            String result = "(cons ";
            result += e.fst().accept(this, env) + " ";
            result += e.snd().accept(this, env);
            return result + ")";
        }

        public String visit(AST.ListExp e, Env env) {
            StringBuilder result = new StringBuilder("(list ");
            for (AST.Exp exp : e.elems())
                result.append(exp.accept(this, env)).append(" ");
            return result + ")";
        }

        public String visit(AST.NullExp e, Env env) {
            String result = "(null? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.NumCheckExp e, Env env) {
            String result = "(number? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.BoolCheckExp e, Env env) {
            String result = "(boolean? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.StrCheckExp e, Env env) {
            String result = "(string? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.PairCheckExp e, Env env) {
            String result = "(pair? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.ListCheckExp e, Env env) {
            String result = "(list? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.UnitCheckExp e, Env env) {
            String result = "(unit? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.ProcCheckExp e, Env env) {
            String result = "(procedure? ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.RefExp e, Env env) {
            String result = "(ref ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.DerefExp e, Env env) {
            String result = "(deref ";
            result += e.arg().accept(this, env);
            return result + ")";
        }

        public String visit(AST.AssignExp e, Env env) {
            String result = "(set! ";
            result += e.fst().accept(this, env) + " ";
            result += e.snd().accept(this, env);
            return result + ")";
        }

        public String visit(AST.FreeExp e, Env env) {
            String result = "(free ";
            result += e.arg().accept(this, env);
            return result + ")";
        }
    }
}
