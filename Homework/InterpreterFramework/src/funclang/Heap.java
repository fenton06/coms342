package funclang;

import funclang.Value.RefVal;

public interface Heap {

    RefVal ref(Value val);

    Value deref(RefVal loc);

    Value setref(RefVal loc, Value val);

    RefVal free(RefVal loc);

    class Heap16Bit implements Heap {

        // 16 bits = 65536 -> 65535 starting at 0
        private Value[] _rep;

        Heap16Bit() {

            _rep = new Value[65535];

            for(int i = 0; i < _rep.length; i++) {
                _rep[i] = null;
            }
        }

        @Override
        public RefVal ref(Value val) {

            int loc = 0;

            while(_rep[loc] != null) {
                loc++;
            }

            _rep[loc] = val;

            return new RefVal(loc);
        }

        @Override
        public Value deref(RefVal loc) {
            return _rep[loc.loc()];
        }

        @Override
        public Value setref(RefVal loc, Value val) {

            _rep[loc.loc()] = val;

            return val;
        }

        @Override
        public RefVal free(RefVal loc) {

            _rep[loc.loc()] = null;

            return loc;
        }
    }

}
