package funclang;

import static funclang.AST.*;
import static funclang.Value.*;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;

import funclang.Env.*;

public class Evaluator implements Visitor<Value> {

    Printer.Formatter ts = new Printer.Formatter();

    Env initEnv = initialEnv(); //New for definelang

    Value valueOf(Program p) {
        return (Value) p.accept(this, initEnv);
    }

    @Override
    public Value visit(AddExp e, Env env) {
        List<Exp> operands = e.all();
        double result = 0;
        for (Exp exp : operands) {
            NumVal intermediate = (NumVal) exp.accept(this, env); // Dynamic type-checking
            result += intermediate.v(); //Semantics of AddExp in terms of the target language.
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(UnitExp e, Env env) {
        return new UnitVal();
    }

    @Override
    public Value visit(NumExp e, Env env) {
        return new NumVal(e.v());
    }

    @Override
    public Value visit(StrExp e, Env env) {
        return new StringVal(e.v());
    }

    @Override
    public Value visit(BoolExp e, Env env) {
        return new BoolVal(e.v());
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Value visit(DivExp e, Env env) {
        List<Exp> operands = e.all();
        NumVal lVal = (NumVal) operands.get(0).accept(this, env);
        double result = lVal.v();
        for (int i = 1; i < operands.size(); i++) {
            NumVal rVal = (NumVal) operands.get(i).accept(this, env);
            result = result / rVal.v();
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(MultExp e, Env env) {
        List<Exp> operands = e.all();
        double result = 1;
        for (Exp exp : operands) {
            NumVal intermediate = (NumVal) exp.accept(this, env); // Dynamic type-checking
            result *= intermediate.v(); //Semantics of MultExp.
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(Program p, Env env) {
        try {
            for (DefineDecl d : p.decls())
                d.accept(this, initEnv);
            return (Value) p.e().accept(this, initEnv);
        } catch (ClassCastException e) {
            return new DynamicError(e.getMessage());
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Value visit(SubExp e, Env env) {
        List<Exp> operands = e.all();
        NumVal lVal = (NumVal) operands.get(0).accept(this, env);
        double result = lVal.v();
        for (int i = 1; i < operands.size(); i++) {
            NumVal rVal = (NumVal) operands.get(i).accept(this, env);
            result = result - rVal.v();
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(VarExp e, Env env) {
        // Previously, all variables had value 42. New semantics.
        return env.get(e.name());
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Value visit(LetExp e, Env env) { // New for varlang.
        List<String> names = e.names();
        List<Exp> value_exps = e.value_exps();
        List<Value> values = new ArrayList<Value>(value_exps.size());

        for (Exp exp : value_exps)
            values.add((Value) exp.accept(this, env));

        Env new_env = env;
        for (int index = 0; index < names.size(); index++)
            new_env = new ExtendEnv(new_env, names.get(index), values.get(index));

        return (Value) e.body().accept(this, new_env);
    }

    @Override
    public Value visit(DefineDecl e, Env env) { // New for definelang.
        String name = e.name();
        Exp value_exp = e.value_exp();
        Value value = (Value) value_exp.accept(this, env);
        ((GlobalEnv) initEnv).extend(name, value);
        return new Value.UnitVal();
    }

    @Override
    public Value visit(LambdaExp e, Env env) { // New for funclang.
        return new Value.FunVal(env, e.formals(), e.defaultVal(), e.body());
    }

    @Override
    public Value visit(CallExp e, Env env) { // New for funclang.
        Object result = e.operator().accept(this, env);

        if (!(result instanceof Value.FunVal))
            return new Value.DynamicError("Operator not a function in call " + ts.visit(e, env));
        Value.FunVal operator = (Value.FunVal) result; //Dynamic checking
        List<Exp> operands = e.operands();

        // Call-by-value semantics
        List<Value> actuals = new ArrayList<>(operands.size());
        for (Exp exp : operands)
            actuals.add((Value) exp.accept(this, env));

        List<String> formals = operator.formals();
        Value defaultVal =  (Value) operator.defaultVal().accept(this, env);

        if (formals.size() != actuals.size()) {
                if(!(defaultVal instanceof StringVal)) { // Checl if default was supplied
                    actuals.add((Value) operator.defaultVal().accept(this, env));
                } else {
                    return new Value.DynamicError("Argument mismatch in call " + ts.visit(e, env));
                }
        }

        Env fun_env = operator.env();
        for (int index = 0; index < formals.size(); index++)
            fun_env = new ExtendEnv(fun_env, formals.get(index), actuals.get(index));

        return (Value) operator.body().accept(this, fun_env);
    }

    @Override
    public Value visit(IfExp e, Env env) { // New for funclang.
        Object result = e.conditional().accept(this, env);
        if (!(result instanceof Value.BoolVal))
            return new Value.DynamicError("Condition not a boolean in expression " + ts.visit(e, env));
        Value.BoolVal condition = (Value.BoolVal) result; //Dynamic checking

        if (condition.v())
            return (Value) e.then_exp().accept(this, env);
        else return (Value) e.else_exp().accept(this, env);
    }

    @Override
    public Value visit(LessExp e, Env env) { // New for funclang.
            Value.NumVal first = (Value.NumVal) e.first_exp().accept(this, env);
            Value.NumVal second = (Value.NumVal) e.second_exp().accept(this, env);
            return new Value.BoolVal(first.v() < second.v());
    }

    @Override
    public Value visit(EqualExp e, Env env) { // New for funclang.

        if ((e.first_exp().accept(this, env) instanceof Value.NumVal) &&
                (e.second_exp().accept(this, env) instanceof Value.NumVal)) {  // Numbers

            Value.NumVal first = (Value.NumVal) e.first_exp().accept(this, env);
            Value.NumVal second = (Value.NumVal) e.second_exp().accept(this, env);

            return new Value.BoolVal(first.v() == second.v());

        } else if ((e.first_exp().accept(this, env) instanceof Value.StringVal) &&
                (e.second_exp().accept(this, env) instanceof Value.StringVal)) { // Strings

            Value.StringVal first = (StringVal) e.first_exp().accept(this, env);
            Value.StringVal second = (StringVal) e.second_exp().accept(this, env);

            return new Value.BoolVal((first.v()).equals(second.v()));

        } else if ((e.first_exp().accept(this, env) instanceof Value.BoolVal) &&
                (e.second_exp().accept(this, env) instanceof Value.BoolVal)) { // Booleans

            Value.BoolVal first = (BoolVal) e.first_exp().accept(this, env);
            Value.BoolVal second = (BoolVal) e.second_exp().accept(this, env);

            return new Value.BoolVal((first.v()) == (second.v()));

        } else if ((e.first_exp().accept(this, env) instanceof Value.PairVal) &&
                (e.second_exp().accept(this, env) instanceof Value.PairVal)) { // List

            Value.PairVal first = (PairVal) e.first_exp().accept(this, env);
            Value.PairVal second = (PairVal) e.second_exp().accept(this, env);

            String firstStr = first.tostring();
            String secondStr = second.tostring();

            return new Value.BoolVal(firstStr.equals(secondStr));

        } else if ((e.first_exp().accept(this, env) instanceof Value.Null) &&
                (e.second_exp().accept(this, env) instanceof Value.Null)) { // Nulls

            return new Value.BoolVal(true);

        } else if ((e.first_exp().accept(this, env) instanceof Value.FunVal) &&
                (e.second_exp().accept(this, env) instanceof Value.FunVal)) {

            Value.FunVal first = (FunVal) e.first_exp().accept(this, env);
            Value.FunVal second = (FunVal) e.second_exp().accept(this, env);

            return new Value.BoolVal(first == second);

        } else { // Nothing matched
            return new Value.BoolVal(false);
        }
    }

    @Override
    public Value visit(GreaterExp e, Env env) { // New for funclang.
        Value.NumVal first = (Value.NumVal) e.first_exp().accept(this, env);
        Value.NumVal second = (Value.NumVal) e.second_exp().accept(this, env);
        return new Value.BoolVal(first.v() > second.v());
    }

    @Override
    public Value visit(CarExp e, Env env) {
        Value.PairVal pair = (Value.PairVal) e.arg().accept(this, env);
        return pair.fst();
    }

    @Override
    public Value visit(CdrExp e, Env env) {
        Value.PairVal pair = (Value.PairVal) e.arg().accept(this, env);
        return pair.snd();
    }

    @Override
    public Value visit(ConsExp e, Env env) {
        Value first = (Value) e.fst().accept(this, env);
        Value second = (Value) e.snd().accept(this, env);
        return new Value.PairVal(first, second);
    }

    @Override
    public Value visit(ListExp e, Env env) { // New for funclang.
        List<Exp> elemExps = e.elems();
        int length = elemExps.size();
        if (length == 0)
            return new Value.Null();

        //Order of evaluation: left to right e.g. (list (+ 3 4) (+ 5 4))
        Value[] elems = new Value[length];
        for (int i = 0; i < length; i++)
            elems[i] = (Value) elemExps.get(i).accept(this, env);

        Value result = new Value.Null();
        for (int i = length - 1; i >= 0; i--)
            result = new PairVal(elems[i], result);
        return result;
    }

    @Override
    public Value visit(NullExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.Null);
    }

    @Override
    public Value visit(NumCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.NumVal);
    }

    @Override
    public Value visit(BoolCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.BoolVal);
    }

    @Override
    public Value visit(StrCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.StringVal);
    }

    @Override
    public Value visit(PairCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.PairVal && !(((PairVal) val).isList()));
    }

    @Override
    public Value visit(ListCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.PairVal && (((PairVal) val).isList()));
    }

    @Override
    public Value visit(UnitCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.UnitVal);
    }

    @Override
    public Value visit(ProcCheckExp e, Env env) {
        Value val = (Value) e.arg().accept(this, env);
        return new BoolVal(val instanceof Value.FunVal);
    }

    @Override
    public RefVal visit(RefExp e, Env env) {

        Value val = (Value) e.arg().accept(this, env);

        return  _heap.ref(val);
    }

    @Override
    public Value visit(DerefExp e, Env env) {

        RefVal loc = (RefVal) e.arg().accept(this, env);

        return _heap.deref(loc);
    }

    @Override
    public Value visit(AssignExp e, Env env) {

        RefVal loc =  (RefVal) e.fst().accept(this, env);

        Value val = (Value) e.snd().accept(this, env);

        _heap.setref(loc, val);

        return val;
    }

    @Override
    public RefVal visit(FreeExp e, Env env) {

        RefVal loc = (RefVal) e.arg().accept(this, env);

        _heap.free(loc);

        return loc;
    }

    public Value visit(EvalExp e, Env env) {
        StringVal programText = (StringVal) e.code().accept(this, env);
        Program p = _reader.parse(programText.v());
        return (Value) p.accept(this, env);
    }

    public Value visit(ReadExp e, Env env) {
        StringVal fileName = (StringVal) e.file().accept(this, env);
        try {
            String text = Reader.readFile("" + System.getProperty("user.dir") + File.separator + fileName.v());
            return new StringVal(text);
        } catch (IOException ex) {
            return new DynamicError(ex.getMessage());
        }
    }

    private Env initialEnv() {
        GlobalEnv initEnv = new GlobalEnv();

		/* Procedure: (read <filename>). Following is same as (define read (lambda (file) (read file))) */
        List<String> formals = new ArrayList<>();
        formals.add("file");
        Exp body = new AST.ReadExp(new VarExp("file"));
        Value.FunVal readFun = new Value.FunVal(initEnv, formals, body);
        initEnv.extend("read", readFun);

		/* Procedure: (require <filename>). Following is same as (define require (lambda (file) (eval (read file)))) */
        formals = new ArrayList<>();
        formals.add("file");
        body = new EvalExp(new AST.ReadExp(new VarExp("file")));
        Value.FunVal requireFun = new Value.FunVal(initEnv, formals, body);
        initEnv.extend("require", requireFun);

		/* Add new built-in procedures here */

        return initEnv;
    }

    Reader _reader;
    Heap _heap;

    public Evaluator(Reader reader, Heap heap) {
        _reader = reader;
        _heap = heap;
    }
}
