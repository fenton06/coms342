(define max
    (lambda (lst)
        (if (null? (cdr lst))
            (car lst)
            (if (< (car lst) (max (cdr lst)))
                (max (cdr lst))
                (car lst)
            )
        )
    )
)

(define min
    (lambda (lst)
        (if (null? (cdr lst))
            (car lst)
            (if (< (car lst) (min (cdr lst)))
                (car lst)
                (min (cdr lst))
            )
        )
    )
)

(define range
    (lambda (lst)
        (- (max lst) (min lst))
    )
)
