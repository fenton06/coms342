(define isOdd
    (lambda (x)
        (if (= x 1)
            #t
            (if (= x 0)
                #f
                (isOdd (- x 2))
            )
        )
    )
)

(define odd
    (lambda (lst)
        (if (null? (cdr lst))
            (if (isOdd (car lst))
                (car lst)
                (cdr lst)
            )
            (if (isOdd (car lst))
                (cons
                    (car lst)
                    (odd (cdr lst))
                )
                (odd (cdr lst))
            )
        )
    )
)
