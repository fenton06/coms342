(define add92
    (lambda (addTo)
        (cons
            (cons 9 2)
            addTo
        )
    )
)