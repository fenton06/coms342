(define isRightAngled
    (lambda (x y z)
        (if (= (* z z) (+ (* x x)(* y y)))
            #t
            #f
        )
    )
)