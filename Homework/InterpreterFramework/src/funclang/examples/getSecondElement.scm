(define getSecondElement
    (lambda (lst)
        (if (null? (cdr lst))
            (list (cdr (car lst)))
            (cons
                (cdr (car lst))
                (getSecondElement (cdr lst))
            )
        )
    )
)
