(define sumN
    (lambda (n)
        (if (> n 0)
            (+ n (sumN(- n 1)))
            0
        )
    )
)