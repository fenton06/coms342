package funclang;

import java.util.ArrayList;
import java.util.List;


/**
 * This class hierarchy represents expressions in the abstract syntax tree
 * manipulated by this interpreter.
 *
 * @author hridesh
 */
@SuppressWarnings("rawtypes")
public interface AST {
    abstract class ASTNode implements AST {
        public abstract Object accept(Visitor visitor, Env env);
    }

    class Program extends ASTNode {
        List<DefineDecl> _decls;
        Exp _e;

        public Program(List<DefineDecl> decls, Exp e) {
            _decls = decls;
            _e = e;
        }

        public Exp e() {
            return _e;
        }

        public List<DefineDecl> decls() {
            return _decls;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    abstract class Exp extends ASTNode {

    }

    class VarExp extends Exp {
        String _name;

        public VarExp(String name) {
            _name = name;
        }

        public String name() {
            return _name;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class UnitExp extends Exp {

        public UnitExp() {
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }

    }

    class NumExp extends Exp {
        double _val;

        public NumExp(double v) {
            _val = v;
        }

        public double v() {
            return _val;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class StrExp extends Exp {
        String _val;

        public StrExp(String v) {
            _val = v;
        }

        public String v() {
            return _val;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class BoolExp extends Exp {
        boolean _val;

        public BoolExp(boolean v) {
            _val = v;
        }

        public boolean v() {
            return _val;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    abstract class CompoundArithExp extends Exp {
        List<Exp> _rest;

        public CompoundArithExp() {
            _rest = new ArrayList<Exp>();
        }

        public CompoundArithExp(Exp fst) {
            _rest = new ArrayList<Exp>();
            _rest.add(fst);
        }

        public CompoundArithExp(List<Exp> args) {
            _rest = new ArrayList<Exp>();
            for (Exp e : args)
                _rest.add((Exp) e);
        }

        public CompoundArithExp(Exp fst, List<Exp> rest) {
            _rest = new ArrayList<Exp>();
            _rest.add(fst);
            _rest.addAll(rest);
        }

        public CompoundArithExp(Exp fst, Exp second) {
            _rest = new ArrayList<Exp>();
            _rest.add(fst);
            _rest.add(second);
        }

        public Exp fst() {
            return _rest.get(0);
        }

        public Exp snd() {
            return _rest.get(1);
        }

        public List<Exp> all() {
            return _rest;
        }

        public void add(Exp e) {
            _rest.add(e);
        }

    }

    class AddExp extends CompoundArithExp {
        public AddExp(Exp fst) {
            super(fst);
        }

        public AddExp(List<Exp> args) {
            super(args);
        }

        public AddExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public AddExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class SubExp extends CompoundArithExp {

        public SubExp(Exp fst) {
            super(fst);
        }

        public SubExp(List<Exp> args) {
            super(args);
        }

        public SubExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public SubExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class DivExp extends CompoundArithExp {
        public DivExp(Exp fst) {
            super(fst);
        }

        public DivExp(List<Exp> args) {
            super(args);
        }

        public DivExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public DivExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class MultExp extends CompoundArithExp {
        public MultExp(Exp fst) {
            super(fst);
        }

        public MultExp(List<Exp> args) {
            super(args);
        }

        public MultExp(Exp fst, List<Exp> rest) {
            super(fst, rest);
        }

        public MultExp(Exp left, Exp right) {
            super(left, right);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A let expression has the syntax
     * <p>
     * (let ((name expression)* ) expression)
     *
     * @author hridesh
     */
    class LetExp extends Exp {
        List<String> _names;
        List<Exp> _value_exps;
        Exp _body;

        public LetExp(List<String> names, List<Exp> value_exps, Exp body) {
            _names = names;
            _value_exps = value_exps;
            _body = body;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }

        public List<String> names() {
            return _names;
        }

        public List<Exp> value_exps() {
            return _value_exps;
        }

        public Exp body() {
            return _body;
        }

    }

    /**
     * A define declaration has the syntax
     * <p>
     * (define name expression)
     *
     * @author hridesh
     */
    class DefineDecl extends Exp {
        String _name;
        Exp _value_exp;

        public DefineDecl(String name, Exp value_exp) {
            _name = name;
            _value_exp = value_exp;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }

        public String name() {
            return _name;
        }

        public Exp value_exp() {
            return _value_exp;
        }

    }

    /**
     * An anonymous procedure declaration has the syntax
     *
     * @author hridesh
     */
    class LambdaExp extends Exp {
        List<String> _formals;
        Exp _body;
        Exp _default;

        public LambdaExp(List<String> formals, Exp body) {
            _formals = formals;
            _body = body;
            _default = new StrExp("");
        }

        // Handle default values
        public LambdaExp(List<String> formals, Exp defaultVal, Exp body) {
            _formals = formals;
            _default = defaultVal;
            _body = body;
        }

        public List<String> formals() {
            return _formals;
        }

        public Exp defaultVal() {
            return _default;
        }

        public Exp body() {
            return _body;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A call expression has the syntax
     *
     * @author hridesh
     */
    class CallExp extends Exp {
        Exp _operator;
        List<Exp> _operands;

        public CallExp(Exp operator, List<Exp> operands) {
            _operator = operator;
            _operands = operands;
        }

        public Exp operator() {
            return _operator;
        }

        public List<Exp> operands() {
            return _operands;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * An if expression has the syntax
     * <p>
     * (if conditional_expression true_expression false_expression)
     *
     * @author hridesh
     */
    class IfExp extends Exp {
        Exp _conditional;
        Exp _then_exp;
        Exp _else_exp;

        public IfExp(Exp conditional, Exp then_exp, Exp else_exp) {
            _conditional = conditional;
            _then_exp = then_exp;
            _else_exp = else_exp;
        }

        public Exp conditional() {
            return _conditional;
        }

        public Exp then_exp() {
            return _then_exp;
        }

        public Exp else_exp() {
            return _else_exp;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A less expression has the syntax
     * <p>
     * ( < first_expression second_expression )
     *
     * @author hridesh
     */
    class LessExp extends BinaryComparator {
        public LessExp(Exp first_exp, Exp second_exp) {
            super(first_exp, second_exp);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    abstract class BinaryComparator extends Exp {
        private Exp _first_exp;
        private Exp _second_exp;

        BinaryComparator(Exp first_exp, Exp second_exp) {
            _first_exp = first_exp;
            _second_exp = second_exp;
        }

        public Exp first_exp() {
            return _first_exp;
        }

        public Exp second_exp() {
            return _second_exp;
        }
    }

    /**
     * An equal expression has the syntax
     * <p>
     * ( == first_expression second_expression )
     *
     * @author hridesh
     */
    class EqualExp extends BinaryComparator {
        public EqualExp(Exp first_exp, Exp second_exp) {
            super(first_exp, second_exp);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A greater expression has the syntax
     * <p>
     * ( > first_expression second_expression )
     *
     * @author hridesh
     */
    class GreaterExp extends BinaryComparator {
        public GreaterExp(Exp first_exp, Exp second_exp) {
            super(first_exp, second_exp);
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A car expression has the syntax
     * <p>
     * ( car expression )
     *
     * @author hridesh
     */
    class CarExp extends Exp {
        private Exp _arg;

        public CarExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A cdr expression has the syntax
     * <p>
     * ( car expression )
     *
     * @author hridesh
     */
    class CdrExp extends Exp {
        private Exp _arg;

        public CdrExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A cons expression has the syntax
     * <p>
     * ( cons expression expression )
     *
     * @author hridesh
     */
    class ConsExp extends Exp {
        private Exp _fst;
        private Exp _snd;

        public ConsExp(Exp fst, Exp snd) {
            _fst = fst;
            _snd = snd;
        }

        public Exp fst() {
            return _fst;
        }

        public Exp snd() {
            return _snd;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A list expression has the syntax
     * <p>
     * ( list expression* )
     *
     * @author hridesh
     */
    class ListExp extends Exp {
        private List<Exp> _elems;

        public ListExp(List<Exp> elems) {
            _elems = elems;
        }

        public List<Exp> elems() {
            return _elems;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A null expression has the syntax
     * <p>
     * (null? expression )
     *
     * @author hridesh
     */
    class NullExp extends Exp {
        private Exp _arg;

        public NullExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A number expression has the syntax
     * <p>
     * (number? expression )
     *
     * @author bfenton
     */
    class NumCheckExp extends Exp {
        private Exp _arg;

        public NumCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A boolean expression has the syntax
     * <p>
     * (boolean? expression )
     *
     * @author bfenton
     */
    class BoolCheckExp extends Exp {
        private Exp _arg;

        public BoolCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A string expression has the syntax
     * <p>
     * (string? expression )
     *
     * @author bfenton
     */
    class StrCheckExp extends Exp {
        private Exp _arg;

        public StrCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A pair expression has the syntax
     * <p>
     * (pair? expression )
     *
     * @author bfenton
     */
    class PairCheckExp extends Exp {
        private Exp _arg;

        public PairCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A list expression has the syntax
     * <p>
     * (list? expression )
     *
     * @author bfenton
     */
    class ListCheckExp extends Exp {
        private Exp _arg;

        public ListCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A unit expression has the syntax
     * <p>
     * (unit? expression )
     *
     * @author bfenton
     */
    class UnitCheckExp extends Exp {
        private Exp _arg;

        public UnitCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * A procedure expression has the syntax
     * <p>
     * (procedure? expression )
     *
     * @author bfenton
     */
    class ProcCheckExp extends Exp {
        private Exp _arg;

        public ProcCheckExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * Eval expression: evaluate the program that is _val
     *
     * @author hridesh
     */
    class EvalExp extends Exp {
        private Exp _code;

        public EvalExp(Exp code) {
            _code = code;
        }

        public Exp code() {
            return _code;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    /**
     * Read expression: reads the file that is _file
     *
     * @author hridesh
     */
    class ReadExp extends Exp {
        private Exp _file;

        public ReadExp(Exp file) {
            _file = file;
        }

        public Exp file() {
            return _file;
        }

        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class RefExp extends Exp {

        private Exp _arg;

        public RefExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        @Override
        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class DerefExp extends Exp {

        private Exp _arg;

        public DerefExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        @Override
        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class AssignExp extends Exp {

        private Exp _fst;
        private Exp _snd;

        public AssignExp(Exp fst, Exp snd) {
            _fst = fst;
            _snd = snd;
        }

        public Exp fst() {
            return _fst;
        }

        public Exp snd() {
            return _snd;
        }

        @Override
        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    class FreeExp extends Exp {

        private Exp _arg;

        public FreeExp(Exp arg) {
            _arg = arg;
        }

        public Exp arg() {
            return _arg;
        }

        @Override
        public Object accept(Visitor visitor, Env env) {
            return visitor.visit(this, env);
        }
    }

    interface Visitor<T> {
        // This interface should contain a signature for each concrete AST node.
        T visit(AST.AddExp e, Env env);

        T visit(AST.UnitExp e, Env env);

        T visit(AST.NumExp e, Env env);

        T visit(AST.StrExp e, Env env);

        T visit(AST.BoolExp e, Env env);

        T visit(AST.DivExp e, Env env);

        T visit(AST.MultExp e, Env env);

        T visit(AST.Program p, Env env);

        T visit(AST.SubExp e, Env env);

        T visit(AST.VarExp e, Env env);

        T visit(AST.LetExp e, Env env); // New for the varlang

        T visit(AST.DefineDecl d, Env env); // New for the definelang

        T visit(AST.ReadExp e, Env env); // New for the funclang

        T visit(AST.EvalExp e, Env env); // New for the funclang

        T visit(AST.LambdaExp e, Env env); // New for the funclang

        T visit(AST.CallExp e, Env env); // New for the funclang

        T visit(AST.IfExp e, Env env); // Additional expressions for convenience

        T visit(AST.LessExp e, Env env); // Additional expressions for convenience

        T visit(AST.EqualExp e, Env env); // Additional expressions for convenience

        T visit(AST.GreaterExp e, Env env); // Additional expressions for convenience

        T visit(AST.CarExp e, Env env); // Additional expressions for convenience

        T visit(AST.CdrExp e, Env env); // Additional expressions for convenience

        T visit(AST.ConsExp e, Env env); // Additional expressions for convenience

        T visit(AST.ListExp e, Env env); // Additional expressions for convenience

        T visit(AST.NullExp e, Env env); // Additional expressions for convenience

        T visit(AST.NumCheckExp e, Env env);

        T visit(AST.BoolCheckExp e, Env env);

        T visit(AST.StrCheckExp e, Env env);

        T visit(AST.PairCheckExp e, Env env);

        T visit(AST.ListCheckExp e, Env env);

        T visit(AST.UnitCheckExp e, Env env);

        T visit(AST.ProcCheckExp e, Env env);

        T visit(AST.RefExp e, Env env);

        T visit(AST.DerefExp e, Env env);

        T visit(AST.AssignExp e, Env env);

        T visit(AST.FreeExp e, Env env);
    }
}
