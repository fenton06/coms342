package arithlang;

import static arithlang.AST.*;
import static arithlang.Value.*;

public class ASTCounter implements Visitor<Value> {

    Value valueOf(Program p) {
        // Value of a program in this language is the value of the expression
        return (Value) p.accept(this);
    }

    @Override
    public Value visit(PrimeExp e) {
        return new NumVal(e.all().size());
    }

    @Override
    public Value visit(NumExp e) {
        return new NumVal(1);
    }

    @Override
    public Value visit(AddExp e) {
        return new NumVal(e.all().size());
    }

    @Override
    public Value visit(SubExp e) {
        return new NumVal(e.all().size());
    }

    @Override
    public Value visit(MultExp e) {
        return new NumVal(e.all().size());
    }

    @Override
    public Value visit(DivExp e) {
        return new NumVal(e.all().size());
    }

    @Override
    public Value visit(Program p) {
        return new NumVal(1);
    }
}
