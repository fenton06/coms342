package arithlang;

import static arithlang.AST.*;
import static arithlang.Value.*;

import java.util.Arrays;
import java.util.List;

public class Evaluator implements Visitor<Value> {

    Printer.Formatter ts = new Printer.Formatter();

    Value valueOf(Program p) {
        // Value of a program in this language is the value of the expression
        return (Value) p.accept(this);
    }

    @Override
    public Value visit(AddExp e) {
        List<Exp> operands = e.all();
        double result = 0;
        for (Exp exp : operands) {
            NumVal intermediate = (NumVal) exp.accept(this); // Dynamic type-checking
            result += intermediate.v(); //Semantics of AddExp in terms of the target language.
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(NumExp e) {
        return new NumVal(e.v());
    }

    @Override
    public Value visit(DivExp e) {
        List<Exp> operands = e.all();
        NumVal lVal = (NumVal) operands.get(0).accept(this);
        double result = lVal.v();
        for (int i = 1; i < operands.size(); i++) {
            NumVal rVal = (NumVal) operands.get(i).accept(this);
            result = result / rVal.v();
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(MultExp e) {
        List<Exp> operands = e.all();
        double result = 1;
        for (Exp exp : operands) {
            NumVal intermediate = (NumVal) exp.accept(this); // Dynamic type-checking
            result *= intermediate.v(); //Semantics of MultExp.
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(Program p) {
        return (Value) p.e().accept(this);
    }

    @Override
    public Value visit(SubExp e) {
        List<Exp> operands = e.all();
        NumVal lVal = (NumVal) operands.get(0).accept(this);
        double result = lVal.v();
        for (int i = 1; i < operands.size(); i++) {
            NumVal rVal = (NumVal) operands.get(i).accept(this);
            result = result - rVal.v();
        }
        return new NumVal(result);
    }

    @Override
    public Value visit(PrimeExp e) {
        List<Exp> operands = e.all();
        NumVal lVal = (NumVal) operands.get(0).accept(this);
        double val = lVal.v();

        // Check for negative numbers
        if (val < 0) {
            System.out.println("Error: Prime operand should not be negative");
            return new NumVal(0);
        }

        int numInt = (int) val;

        // Check for double
        if (numInt != val) {
            return new NumVal(0);
        }

        // Sieve of Erasthenes
        Boolean[] primes = new Boolean[numInt + 1];
        Arrays.fill(primes, true);

        for (int i = 2; i * i <= numInt; i++) {
            if (primes[i]) {
                for (int j = i * 2; j <= numInt; j += i)
                    primes[j] = false;
            }
        }

        if (primes[numInt]) {
            return new NumVal(1);
        } else {
            return new NumVal(0);
        }
    }
}
